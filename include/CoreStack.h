#ifndef CORESTACK_H
#define CORESTACK_H

#include <common.h>

value_type CORE_STACK[MAX_STACK];
size_t CORE_STACK_INDEX;

static bool initialise_core_stack(){
    size_t i;
    for( i = 0; i < MAX_STACK; i++ ){
        CORE_STACK[i] = 0;
    }

    CORE_STACK_INDEX = 0;

    return true;
}

#endif
