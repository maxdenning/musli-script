#ifndef DEFINITIONS_ARITHMETIC_H
#define DEFINITIONS_ARITHMETIC_H

#include "common.h"
#include "Syntax.h"

bool exe_ARITHMETIC_ADDITION_   ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_ARITHMETIC_SUBTRACT_   ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_ARITHMETIC_MULTIPLY_   ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_ARITHMETIC_DIVIDE_     ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_ARITHMETIC_MOD_        ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );

bool spt_ARITHMETIC_CLEANUP_    ( value_type stack_[], size_t* si_ );

#endif
