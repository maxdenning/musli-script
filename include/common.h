#ifndef COMMON_H
#define COMMON_H

/* Contains functionality intended to be common between all files */

/* Libs */
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>


/* Definitions */
#define DEBUG       0                                   /* Flags whether to execute debugging code */
#define D_PRINT_ALL 1                                   /* Flags whether to print executable output after execution when debugging */

#define MAX_STACK   128                                 /* Number of cells in each array stack */
#define MAX_SYNTAX  28                                  /* Number of syntax words specified */
#define MAX_STR     MAX_STACK > 16 ? 16 : MAX_STACK     /* Maximum length of multi-digit numbers, capped at 16 */
#define FALSE_VAL   0                                   /* Comparison value for boolean operations */
#define VALUE_TYPE  float                               /* Data type of CORE_STACK */
#define TYPE_FORMAT "f"                                 /* printf formatting character for VALUE_TYPE */

typedef VALUE_TYPE value_type;

/* Functions */
static bool printErr( const char file_name_[], const char function_name_[], const char msg_[] ){
    printf(" --- Error in %s::%s --- \nMessage: %s\n", file_name_, function_name_, msg_);
    return false;
}

#endif
