#ifndef SYNTAX_H
#define SYNTAX_H

#include "common.h"

/* Data */
typedef enum {
    Verb,
    Value,
    Branch,
    Control,
    Arithmetic,
    Logical
} WordType;

typedef enum{
    /*  --- VERBS --------  */
    Push = 0,
    Pop  = 1,
    Peek = 2,
    Incr = 3,
    Decr = 4,
    Goto = 5,
    Copy = 6,

    /*  --- VALUES -------  */
    Index    = 7,
    Cast     = 8,
    NewStack = 9,
    ValueAt  = 10,
    AsCore   = 11,
    Rand     = 12,

    /*  --- BRANCH -------  */
    If     = 13,
    Loop   = 14,
    Group  = 15,
    Switch = 16,

    /*  --- Control ------  */
    EndGroup    = 17,
    EndNewStack = 18,

    /*  --- ARITHMETIC ---  */
    Add  = 19,
    Subt = 20,
    Mult = 21,
    Divi = 22,
    Mod  = 23,

    /* --- LOGICAL -------- */
    And = 24,
    Or  = 25,

    /* --- EXCLUDED ------- */
    Comment = 26,
    WSDeref = 27

} SyntaxLabel;


typedef bool (*ExecutePtr)(int stack_[], int* sidx_, struct Word* context_, long* cidx_);

typedef struct {
    char symbol;
    WordType type;
    ExecutePtr execute;
    value_type value;
} Word;

Word syntax[ MAX_SYNTAX ];


/* Functions */
bool addRule( size_t index_, char symbol_, WordType type_, const ExecutePtr* exe_func_ );
void toWords( Word* words_, const char* executable_text_, size_t* out_length_ );

bool isSyntax( const Word* word_, SyntaxLabel label_ );
bool isType( const Word* word_, WordType type_ );
bool isTrueW( const Word* word_ );
bool isTrueV( const value_type* value_ );

bool exe_VALUE_PLACEHOLDER_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );

#endif
