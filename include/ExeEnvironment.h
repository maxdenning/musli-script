#ifndef EXEENVIRONMENT_H
#define EXEENVIRONMENT_H

#include "common.h"
#include "Syntax.h"
#include "Definitions_Verbs.h"
#include "Definitions_Values.h"
#include "Definitions_Branch.h"
#include "Definitions_Control.h"
#include "Definitions_Arithmetic.h"
#include "Definitions_Logical.h"
#include "CoreStack.h"

bool initialise_syntax();
bool executeFromText( const char* executable_text_, size_t length_ );

#endif
