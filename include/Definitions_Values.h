#ifndef DEFINITIONS_VALUES_H
#define DEFINITIONS_VALUES_H

#include "common.h"
#include "Syntax.h"
#include "CoreStack.h"

bool exe_VALUE_STACK_INDEX_ ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_VALUE_CAST_        ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_VALUE_NEW_STACK_   ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_VALUE_AT_          ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_VALUE_AS_CORE_     ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_VALUE_RAND_        ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );

#endif
