#ifndef DEFINITIONS_CONTROL_H
#define DEFINITIONS_CONTROL_H

#include "common.h"
#include "Syntax.h"

bool exe_CONTROL_GROUPS_    ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_CONTROL_NEW_STACK_ ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );

#endif
