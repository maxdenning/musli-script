#ifndef TEXTHANDLER_H
#define TEXTHANDLER_H

#include <unistd.h>
#include "common.h"
#include "Syntax.h"

/*
** TextHandler contains functions which take user input for validation and sanitation, and
** handles other text/file based tasks.
*/

/* Functions */
bool validateArgs( int argc_, char* argv_[] );
bool loadTextFile( const char* file_path, char** out_file_content_, long* out_file_length_ );
void removeWhitespace( char** out_text_, const long* prev_text_length_, long* out_new_length_ );


#endif
