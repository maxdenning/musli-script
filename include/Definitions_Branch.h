#ifndef DEFINITIONS_BRANCH_H
#define DEFINITIONS_BRANCH_H

#include "common.h"
#include "Syntax.h"

bool exe_BRANCH_IF_     ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_BRANCH_LOOP_   ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_BRANCH_GROUPS_ ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_BRANCH_SWITCH_ ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );

bool spt_BRANCH_INIT_   ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool spt_BRANCH_EXP_MOV_( Word context_[], size_t* ci_, int ignore_cl_, int ignore_scl_  );

#endif
