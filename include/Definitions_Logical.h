#ifndef DEFINITIONS_LOGICAL_H
#define DEFINITIONS_LOGICAL_H

#include "common.h"
#include "Syntax.h"

bool exe_LOGICAL_AND_ ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_LOGICAL_OR_  ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );

bool spt_LOGICAL_CLEANUP_( value_type stack_[], size_t* si_ );

#endif
