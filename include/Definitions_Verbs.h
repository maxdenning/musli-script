#ifndef DEFINITIONS_VERBS_H
#define DEFINITIONS_VERBS_H

#include <limits.h>
#include "common.h"
#include "Syntax.h"

bool exe_VERB_PUSH_    ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_VERB_POP_     ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_VERB_PEEK_    ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_VERB_INCR_    ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_VERB_DECR_    ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_VERB_GOTO_    ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );
bool exe_VERB_COPY_TO_ ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ );

#endif
