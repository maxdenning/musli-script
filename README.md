# MUSLI Script
A simple language syntax and script interpreter.


## Execution
To execute a script, simply run the MUSLI interpreter with the path to the script file as the only argument


## Syntax
### Verbs
| Keyword | Symbol | Description |
| --- | --- | :--- |
| Push | > |  Incrememnts the current index and then inserts the given value. If no value is given, `0` is inserted. |
| Pop | < |  Sets the cell at the current index to `0`, and then decrements the current index. |
| Peek | = |  Prints the value of the cell at the current index. |
| Increment | . |  Increments the value of the cell at the current index. |
| Decrement | , | Decrements the value of the cell at the current index. |
| Go to | ^ | Sets the current index to the given value.
| Copy to | " |  Copies the value of the cell at the current index to the given index.

### Values
| Keyword | Symbol | Description |
| --- | --- | :--- |
| Current index | # | Returns the current index. | 
| Cast | ' | Takes the character of the given value, i.e. `>a` is `97`, whereas `>'a` is `a`. Can be combined with Peek(`=`) to print characters instead of numbers. |
| New stack | [ | Opens a new array instance. Returns the value of the cell at its own current index. |
| Value at | $ | Returns the value of the cell at the given index. |
| As core | ` | Performs the next operation as the core stack, i.e. `[>$0]` pushes the value of the substack's 0th cell to the substack, whereas [>`$0] pushes the value of the core stack's 0th cell to the substack. |
| Random nubmer | ! | Returns a random number. |

### Branch
| Keyword | Symbol | Description |
| --- | --- | :--- |
| If | ? | If the given value is greater than `0` it will execute the 'true' block. |
| Loop | @ | While the given value is greater than `0` it will execute the loop block. |
| Branch group | : | Used to capture the blocks for If(`?`), Loop(`@`), and Switch(`~`). `?1: true : false ;` `@1: loop block :; ~2: skip : skip : execute ;`. |
| Switch | ~ | Jumps to the selected block, e.g. `~2: skip : skip : execute ;`. |

### Control
| Keyword | Symbol | Description |
| --- | --- | :--- |
| Close branch group | ; | Closes the current branch group, e.g. `?1: true : false ;`. |
| Close new stack | ] | Closes the substack, e.g. `[>1]`. |

### Arithmetic Operators
| Keyword | Symbol | Description |
| --- | --- | :--- |
| Add | + | Add the previous two array cells. |
| Subtract | - | Subtracts the previous cell from the one before it, e.g. `>8>10>-` is `-2`. |
| Multiply | * | Multiplies the two previous cells. |
| Divide | / | Divides the previous cell by the one before it. |
| Mod | _ | Calculates the modulus of the two previous cells, as with Division(`/`). |

### Logic Operators
| Keyword | Symbol | Description |
| --- | --- | :--- |
| And | & | |
| Or | \| | |

### Misc
| Keyword | Symbol | Description |
| --- | --- | :--- |
| Comment | \% | Allows line comments, must be terminated by a \n or \r. |
| Whitespace dereference | \ | Marks whitespace to be ignored by whitespace removal stage, e.g. `> ` is `0`, whereas `>\ ` is ` `. |
