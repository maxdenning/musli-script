#include "Definitions_Logical.h"

static const char* TAG = "Definitions_Logical";

bool exe_LOGICAL_AND_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    if( *si_ < 1 ) return printErr(TAG, "exe_LOGICAL_AND_", "Only 1 value in stack (si_ < 1)");

    context_[*ci_].value = ( isTrueV( &stack_[*si_] ) && isTrueV( &stack_[*si_-1] ) );
    spt_LOGICAL_CLEANUP_( stack_, si_ );

    (*ci_)++;
    return true;
}

bool exe_LOGICAL_OR_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){ /* !!! */
    if( *si_ < 1 ) return printErr(TAG, "exe_LOGICAL_OR_", "Only 1 value in stack (si_ < 1)");

    context_[*ci_].value = ( isTrueV( &stack_[*si_] ) || isTrueV( &stack_[*si_-1] ) );
    spt_LOGICAL_CLEANUP_( stack_, si_ );

    (*ci_)++;
    return true;
}

bool spt_LOGICAL_CLEANUP_( value_type stack_[], size_t* si_ ){
    stack_[*si_] = 0;
    stack_[*si_-1] = 0;
    (*si_) -= 2;
    return true;
}

