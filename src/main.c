#include <stdlib.h>

#include "common.h"
#include "TextHandler.h"
#include "ExeEnvironment.h"
#include <limits.h>

int main(int argc, char* argv[])
{
    /* Initialise */
    char* original_input,                       /* String containing entire script file */
        * executable_input;                     /* String containing stripped down original_input, no whitespace etc */
    long file_length = 0,                       /* Length of original_input */
         executable_length = 0;                 /* Length of executable_input */

    if( !initialise_syntax() ) return 1;        /* Initialise language rules, see ExeEnvironment.c */
    if( !initialise_core_stack() ) return 1;    /* Initialise array for use during execution */


    /*////////////////////////////////////  Get File Input  ////////////////////////////////////*/
    /* Validate input */
    if( !validateArgs( argc, argv ) ) return 1;

    /* Load input file text */
    if( !loadTextFile( argv[1], &original_input, &file_length ) ) return 1;

    /* Copy script input & sanitise for execution */
    executable_input = (char*)malloc( file_length+1 );
    memcpy( executable_input, original_input, file_length );
    removeWhitespace(&executable_input, &file_length, &executable_length);


    /*////////////////////////////////////  Execute  ///////////////////////////////////////////*/
    srand( time( NULL ) );
    executeFromText( executable_input, executable_length );


    /*////////////////////////////////////  Close  /////////////////////////////////////////////*/

    #if DEBUG == 1 && D_PRINT_ALL == 1
        printf( "[DEBUG] Executed text:  " );
        long i;
        for( i = 0; i < executable_length; i++ ){
            printf( "%c", executable_input[i] );
        }
    #endif

    free( original_input );
    free(executable_input);
    return 0;
}

