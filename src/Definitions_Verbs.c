#include "Definitions_Verbs.h"

static const char* TAG = "Definitions_Verbs";

bool exe_VERB_PUSH_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    if( *si_ >= MAX_STACK-1 ) return printErr( TAG, "exe_VERB_PUSH_", "Stack overflow!" );
    long orig_ci_ = ++(*ci_); /* increment context index */

    /* check next Word type
    ** if Value or Arithmetic, execute it and add its value to the stack
    ** otherwise, add a 0 to the stack
    */

    switch( context_[*ci_].type ){
    case Value:
    case Arithmetic:
    case Logical:

            if( !context_[*ci_].execute(stack_, si_, context_, ci_) ){
                return printErr( TAG, "exe_VERB_PUSH_", "Failed to execute pushed value" );
            }

            (*si_)++; /* increment stack index */
            stack_[*si_] = context_[orig_ci_].value;
        break;

    default:
        (*si_)++; /* increment stack index */
        stack_[*si_] = 0;
        break;
    }

    return true;
}

bool exe_VERB_POP_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    size_t orig_ci = ++(*ci_);

    /* check if index value is given, move to it */
    if( isType( &context_[*ci_], Value) ){
        if( !context_[*ci_].execute(stack_, si_, context_, ci_) ){
            return printErr( TAG, "exe_VERB_POP_", "Failed to execute pushed value" );
        }

        size_t index = context_[orig_ci].value;
        if( index < 0 || index >= MAX_STACK-1 ){
            return printErr( TAG, "exe_VERB_POP_", "Stack index out of range" );
        }

        *si_ = index;
    }

    /* reset current index */
    stack_[*si_] = 0;

    /* move all values above down by one index */
    size_t i = 0;
    for( i = *si_+1; i < MAX_STACK; i++ ){
        stack_[i-1] = stack_[i];
    }

    (*si_)--;
    return true;
}

bool exe_VERB_PEEK_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    if( *si_ < 0 ) return printErr( TAG, "exe_VERB_PEEK_", "Stack is empty! (si_ < 0)" );

    /* check next Word
    ** if ', convert printed value to char
    ** otherwise, print as int
    */

    (*ci_)++;

    if( isSyntax( &context_[*ci_], Cast ) && stack_[*si_] < 128 && stack_[*si_] >= 0 ){
        printf( "%c", (int)stack_[*si_] );
        (*ci_)++;
    }else{
        printf( "%" TYPE_FORMAT , stack_[*si_] );
    }

    return true;
}

bool exe_VERB_INCR_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    if( *si_ < 0 ) return printErr( TAG, "exe_VERB_INCR_", "Stack is empty! (si_ < 0)" );

    (stack_[*si_])++;
    value_type t_ = stack_[*si_];
    (*ci_)++;
    return true;
}

bool exe_VERB_DECR_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    if( *si_ < 0 ) return printErr(TAG, "exe_VERB_DECR_", "Stack is empty! (si_ < 0)");

    (stack_[*si_])--;
    (*ci_)++;
    return true;
}

bool exe_VERB_GOTO_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    size_t value = 0;
    size_t orig_ci = ++(*ci_);

         if( isSyntax( &context_[*ci_], Decr ) ){ value = *si_ - 1; (*ci_)++; }
    else if( isSyntax( &context_[*ci_], Incr ) ){ value = *si_ + 1; (*ci_)++; }

    else if( isType( &context_[*ci_], Value ) ){

        if( !context_[*ci_].execute(stack_, si_, context_, ci_) ){
            return printErr( TAG, "exe_VERB_GOTO_", "Failed to execute value" );
        }
        value = context_[ orig_ci ].value;

    }else{
        value = stack_[*si_];
    }


    if( value < 0 || value >= MAX_STACK-1 ){
        return printErr( TAG, "exe_VERB_GOTO_", "Stack index out of range" );
    }
    *si_ = value;
    return true;
}

bool exe_VERB_COPY_TO_ ( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    /*
    ** copy the current index to the given cell
    ** if none given, copy to 0
    */

    size_t orig_ci = ++(*ci_);
    size_t index = 0;

    if( isType( &context_[*ci_], Value ) ){
        if( !context_[*ci_].execute(stack_, si_, context_, ci_) ){
            return printErr( TAG, "exe_VERB_GOTO_", "Failed to execute value" );
        }
        index = context_[orig_ci].value;
    }

    if( index < 0 || index >= MAX_STACK-1 ){
        return printErr( TAG, "exe_VERB_GOTO_", "Stack index out of range" );
    }
    stack_[ index ] = *si_;

    return true;
}

