#include "Definitions_Branch.h"

static const char* TAG = "Definitions_Branch";

bool exe_BRANCH_IF_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    /*
    ** check if next Word is Value and execute it
    ** if 'value' > 0 (true), go to first ':', and execute until the next ':'
    ** then jump to the final ':'
    ** otherwise (false), jump to second ':' and execute to final ':'
    */

    long orig_ci_ = ++(*ci_);
    if( !spt_BRANCH_INIT_(stack_, si_, context_, ci_) ) return false;


    /*//////////////////////////////////////////////////////////
    ///////////////////////  EXECUTE IF  /////////////////////*/

    /* set the expression to be executed */
    if( isTrueW( &context_[orig_ci_] ) ){
        /* TRUE */
        context_[*ci_].value = 0; /* execute first block in expression group */
    }else{
        /* FALSE */
        context_[*ci_].value = 1; /* execute second block in expression group */
    }

    /* execute the expression group */
    if( !context_[*ci_].execute(stack_, si_, context_, ci_) ){
        return printErr(TAG, "exe_BRANCH_IF_", "Expression group execution failed");
    }

    return true;
}

bool exe_BRANCH_LOOP_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    /*
    ** check if next Word is Value and execute it
    ** if 'value' > 0 (true), go to first ':', and execute it
    ** return to the initial Value and execute
    ** continue until false
    **
    ** when false jump to second ':' and execute to final ':'
    */

    long orig_ci_ = ++(*ci_);
    if( !spt_BRANCH_INIT_( stack_, si_, context_, ci_ ) ) return false;


    /*//////////////////////////////////////////////////////////
    //////////////////////  EXECUTE LOOP  ////////////////////*/

    while( isTrueW( &context_[orig_ci_] ) /*loop_value > FALSE_VAL*/ ){
        /* execute the TRUE expression
        ** and then re-evaluate the loop_value
        */

        context_[*ci_].value = 0; /* select TRUE expression */
        if( !context_[*ci_].execute( stack_, si_, context_, ci_ ) ){
            return printErr(TAG, "exe_BRANCH_LOOP_", "Expression group execution failed");
        }

        /* move ci_ back to start and re-evaluate value */
        (*ci_) = orig_ci_;
        if( !context_[*ci_].execute( stack_, si_, context_, ci_ ) ){
            return printErr(TAG, "exe_BRANCH_LOOP_", "Value execution failed");
        }
    }


    /* once false, execute FALSE expression */
    context_[*ci_].value = 1;
    if( !context_[*ci_].execute( stack_, si_, context_, ci_ ) ){
        return printErr(TAG, "exe_BRANCH_LOOP_", "Expression group execution failed");
    }

    return true;
}

bool exe_BRANCH_GROUPS_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    /*
    ** execute until the grouped ':' or ';' is met, in the case of
    ** a ':' jump to the closing ';'
    **
    ** this takes into account branching operators allowing
    ** ':' to executed correctly when nested
    **
    ** the value of the Word is used to determine how many
    ** ':' should be ignored before execution
    **
    ** eg. ?1:TRUE:FALSE;, TRUE is to be executed, therefore 'value' = 0
    **          meaning no ':' are ignored
    **
    **`    ?0:TRUE:FALSE;, FALSE is to be executed, therefore 'value' = 1
    **          meaning the first ':' is ignored
    **
    **      by default 'value' = 0
    */

    if( !spt_BRANCH_EXP_MOV_( context_, ci_, context_[*ci_].value + 1, 0 ) ) /* move to chosen block */
        return printErr( TAG, "exe_BRANCH_GROUPS_", "Expression block execution failed" );

    /* execute until next grouped ':' or grouped ';'
    **      the execution loop does not need to be sensitive to nested
    **      branching operators as they should consume themselves
    */

    while( !isSyntax( &context_[*ci_], Group) && !isSyntax(&context_[*ci_], EndGroup ) ){
        if( !context_[*ci_].execute( stack_, si_, context_, ci_ ) ){
            return printErr(TAG, "exe_BRANCH_GROUPS_", "Expression block execution failed");
        }
    }


    /* find closing ';' (if not already found)
    ** ';' MUST be consumed!
    */
    spt_BRANCH_EXP_MOV_( context_, ci_, 0, 1 );

    return true;
}

bool exe_BRANCH_SWITCH_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    /*
    ** takes the value to its right, and assigns it to the group starter
    ** to the right of that, then executes that group.
    ** intended to be used for switch cases:
    **      ~2: SKIP : SKIP : EXECUTE ;
    **
    */

    long orig_ci_ = ++(*ci_);
    if( !spt_BRANCH_INIT_( stack_, si_, context_, ci_ ) ) return false;

    /* execute the expression group */
    context_[*ci_].value = context_[orig_ci_].value;
    if( !context_[*ci_].execute( stack_, si_, context_, ci_ ) ){
        return printErr( TAG, "exe_BRANCH_IF_", "Expression group execution failed" );
    }

    return true;
}



/*///////////////////////////////////////////////////////////////////////////
////////////////////////////  SUPPORT FUNCTIONS  ////////////////////////////
///////////////////////////////////////////////////////////////////////////*/

bool spt_BRANCH_INIT_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    /*
    ** takes context beginning on the value Word of the
    ** branch operation
    */

    /* validate correct context given */
    if( context_[*ci_].type != Value ){
        return printErr( TAG, "spt_BRANCH_INIT_", "Formatting incorrect, cannot find value" );
    }

    /* get the value */
    if( !context_[*ci_].execute(stack_, si_, context_, ci_) ){
        return printErr( TAG, "spt_BRANCH_INIT_", "Value execution failed" );
    }

    /* execution will move the context up to the beginning of the expression group */
    if( !isSyntax(&context_[*ci_], Group) ){
        return printErr( TAG, "spt_BRANCH_INIT_", "Formatting incorrect, cannot find first clause" );
    }

    return true;
}

bool spt_BRANCH_EXP_MOV_( Word context_[], size_t* ci_, int ignore_cl_, int ignore_scl_ ){
    /*
    ** moves through an expression group ignoring nested branching
    ** using the given ignore_x_ values to know when to stop
    ** (ie. which block to stop in, or if the end should be found)
    */

    do{
        Word w_ = context_[*ci_];

        if( isSyntax( &context_[*ci_], Group ) ){
            ignore_cl_--;
        }
        else if( isSyntax( &context_[*ci_], EndGroup ) ){
            ignore_scl_--;
        }
        else if( isSyntax( &context_[*ci_], If ) || isSyntax( &context_[*ci_], Loop ) ){
            ignore_cl_ += 2;
            ignore_scl_ += 1;
        }

        (*ci_)++;

    }while( ignore_cl_ > 0 || ignore_scl_ > 0 );

    return ignore_cl_ <= 0;
}

