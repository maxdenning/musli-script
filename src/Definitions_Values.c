#include "Definitions_Values.h"

static const char* TAG = "Definitions_Values";

bool exe_VALUE_STACK_INDEX_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    /* return non-zero index (magnitude) */
    context_[*ci_].value = (*si_);
    (*ci_)++;
    return true;
}

bool exe_VALUE_CAST_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    /*
    ** Takes the ASCII char value of the next Word
    ** and then skips it, meaning the Word is
    ** NOT executed
    */

    context_[*ci_].value = context_[*ci_+1].symbol;
    (*ci_)++;
    (*ci_)++;

    return true;
}

bool exe_VALUE_NEW_STACK_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    /*
    ** open a new stack
    ** execute the following code within the new stack's environment
    ** stop when the closing ']' is found, allowing for nesting
    ** when closed, set value of opening '[' to the top of the temporary stack
    */

    value_type temporary_stack[ MAX_STACK ] = {0};
    size_t temporary_stack_index = 0, orig_ci = *ci_;
    int ignore_closing = 0;

    (*ci_)++;

    while( ignore_closing >= 0 ){
        if( isSyntax(&context_[*ci_], NewStack) ){
            ignore_closing++;
        }
        else if( isSyntax(&context_[*ci_], EndNewStack) ){
            ignore_closing--;
        }

        if( !context_[*ci_].execute( &temporary_stack, &temporary_stack_index, context_, ci_ ) ){
            return printErr( TAG, "exe_VALUE_NEW_STACK_", "Execution failed within temporary stack environment" );
        }
    }

    context_[orig_ci].value = temporary_stack[ temporary_stack_index ];
    return true;
}

bool exe_VALUE_AT_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    /*
    ** check next Word
    ** if Value, execute and use set self.value to stack_[other.value]
    */

    size_t index = *si_;
    long orig_ci = (*ci_)++;

    if( isType( &context_[*ci_], Value ) ){
        if( !context_[*ci_].execute( stack_, si_, context_, ci_ ) ){
            return printErr( TAG, "exe_VALUE_AT_", "Value execution failed" );
        }
        index = context_[orig_ci+1].value;
    }


    if( index >= MAX_STACK ){
        return printErr( TAG, "exe_VALUE_AT_", "Index value >= MAX_STACK" );
    }

    context_[orig_ci].value = stack_[index];
    return true;
}

bool exe_VALUE_AS_CORE_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    /*
    ** execute the next word within the CORE environment
    */

    long orig_ci = ++(*ci_);
    if( !context_[*ci_].execute( &CORE_STACK, &CORE_STACK_INDEX, context_, ci_ ) ){
        return printErr( TAG, "exe_VALUE_AS_CORE_", "Execution in CORE environment failed" );
    }

    context_[orig_ci-1].value = context_[orig_ci].value;
    return true;
}

bool exe_VALUE_RAND_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    /*
    ** generate a random integer and set as value
    */
    context_[*ci_].value = rand();
    (*ci_)++;
    return true;
}

