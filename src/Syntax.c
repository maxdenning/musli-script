#include "Syntax.h"

static const char* TAG = "Syntax";

bool addRule( size_t index_, char symbol_, WordType type_, const ExecutePtr* exe_func_ ){
    if( index_ > MAX_SYNTAX ){
        printErr(TAG, "addRule", "Given index is greater than array size");
        return false;
    }

    syntax[ index_ ] = (Word){ symbol_, type_, exe_func_, 0 };
    return true;
}

void toWords( Word* words_, const char* executable_text_, size_t* out_length_ ){
    long length = 0;

    if( *out_length_ < 1 ) return printErr(TAG, "toWords", "Given out_length_ < 1");

    long w = 0;
    for( w; w < *out_length_; w++ ){
        char* text = &executable_text_[ w ];
        int i = 0, value = 0;
        char multidigit[ MAX_STR ];

        /* check for keyword values */
        for( i = 0; i < MAX_SYNTAX; i++ ){
            if( *text == syntax[i].symbol ) break;
        }
        if( i < MAX_SYNTAX ){
            (words_)[ length ] = syntax[i];
            length++;
            continue;
        }


        /* check for multi-digit values */
        for( i = 0; i < MAX_STR; i++ ){
            char* symbol = &executable_text_[w + i];

            if( *symbol >= 48 && *symbol <= 57 ) multidigit[i] = *symbol;
            else break;
        }
        if( i > 0 ){
            multidigit[i] = '\0';
            value = atoi( multidigit );
            w += i-1;
        }else{
            value = executable_text_[w + i];
        }

        (words_)[ length ] = (Word){ 'd', Value, exe_VALUE_PLACEHOLDER_, value };
        length++;
    }

    long l_ = *out_length_;
    *out_length_ = length;
}

bool exe_VALUE_PLACEHOLDER_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    /* pull 'value' from the 'symbol'
    ** if 0 - 9, 'value' is actual number
    **           a search is conducted for multi-digit numbers
    **           (to add acsii value, a ' must be used)
    ** otherwise, 'value' is the ASCII value
    */

    /*if( context_[*ci_].symbol >= 48 && context_[*ci_].symbol <= 57 ){
        context_[*ci_].value = context_[*ci_].symbol - 48;

    }else{
        context_[*ci_].value = context_[*ci_].symbol;
    }
    (*ci_)++;*/

    (*ci_)++;

    return true;
}

bool isSyntax( const Word* word_, SyntaxLabel label_ ){
    return word_->symbol == syntax[label_].symbol
        && word_->type   == syntax[label_].type;
}

bool isType( const Word* word_, WordType type_ ){
    return word_->type == type_;
}

bool isTrueW( const Word* word_){
    return word_->value > FALSE_VAL;
}

bool isTrueV( const value_type* value_ ){
    return *value_ > FALSE_VAL;
}

