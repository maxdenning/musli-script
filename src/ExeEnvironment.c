#include "ExeEnvironment.h"

static const char* TAG = "ExeEnvironment";

bool initialise_syntax(){
    /* add each syntax rule */

    bool flag = false;

    flag = addRule( Push,        '>',    Verb,          exe_VERB_PUSH_    );
    flag = addRule( Pop,         '<',    Verb,          exe_VERB_POP_     );
    flag = addRule( Peek,        '=',    Verb,          exe_VERB_PEEK_    );
    flag = addRule( Incr,        '.',    Verb,          exe_VERB_INCR_    );
    flag = addRule( Decr,        ',',    Verb,          exe_VERB_DECR_    );
    flag = addRule( Goto,        '^',    Verb,          exe_VERB_GOTO_    );
    flag = addRule( Copy,        '"',    Verb,          exe_VERB_COPY_TO_ );

    flag = addRule( Index,       '#',    Value,         exe_VALUE_STACK_INDEX_ );
    flag = addRule( Cast,        '\'',   Value,         exe_VALUE_CAST_        );
    flag = addRule( NewStack,    '[',    Value,         exe_VALUE_NEW_STACK_   );
    flag = addRule( ValueAt,     '$',    Value,         exe_VALUE_AT_          );
    flag = addRule( AsCore,      '`',    Value,         exe_VALUE_AS_CORE_     );
    flag = addRule( Rand,        '!',    Value,         exe_VALUE_RAND_        );

    flag = addRule( If,          '?',    Branch,        exe_BRANCH_IF_     );
    flag = addRule( Loop,        '@',    Branch,        exe_BRANCH_LOOP_   );
    flag = addRule( Group,       ':',    Branch,        exe_BRANCH_GROUPS_ );
    flag = addRule( Switch,      '~',    Branch,        exe_BRANCH_SWITCH_ );

    flag = addRule( EndGroup,    ';',    Control,       exe_CONTROL_GROUPS_    );
    flag = addRule( EndNewStack, ']',    Control,       exe_CONTROL_NEW_STACK_ );

    flag = addRule( Add,         '+',    Arithmetic,    exe_ARITHMETIC_ADDITION_ );
    flag = addRule( Subt,        '-',    Arithmetic,    exe_ARITHMETIC_SUBTRACT_ );
    flag = addRule( Mult,        '*',    Arithmetic,    exe_ARITHMETIC_MULTIPLY_ );
    flag = addRule( Divi,        '/',    Arithmetic,    exe_ARITHMETIC_DIVIDE_   );
    flag = addRule( Mod,         '_',    Arithmetic,    exe_ARITHMETIC_MOD_      );

    flag = addRule( And,         '&',    Logical,       exe_LOGICAL_AND_ );
    flag = addRule( Or,          '|',    Logical,       exe_LOGICAL_OR_  );

    flag = addRule( Comment,      '%',   Value,         exe_VALUE_PLACEHOLDER_   );
    flag = addRule( WSDeref,      '\\',  Value,         exe_VALUE_PLACEHOLDER_   );

    return flag;
}

bool executeFromText( const char* executable_text_, size_t length_ ){
    /* Initialise */
    Word words[ length_ ];
    long i = 0;

    /* Checks */
    if( length_ < 1 ) return printErr( TAG, "executeFromText", "Given length_ < 1" );

    /* Convert executable_text_ to words */
    toWords( &words, executable_text_, &length_ );


    /* Execution loop */
    i = 0;
    while( i < length_ ){
        if( !words[i].execute( &CORE_STACK, &CORE_STACK_INDEX, words, &i ) ){
            printErr( TAG, "executeFromText", "Execution failed" );
            break;
        }
    }


    #if DEBUG == 1
        printf("\n\n[DEBUG] Core stack:\n");
        for(i = 0; i < MAX_STACK; i++){
            printf("[%" TYPE_FORMAT "]", CORE_STACK[i]);
        } printf("\n");

        #if D_PRINT_ALL == 1
            printf("[DEBUG] Executed words:  ");
            for( i = 0; i < length_; i++ ){
                printf( "(%c, %" TYPE_FORMAT ")", words[i].symbol, words[i].value );
            } printf("\n");
        #endif
    #endif

    return true;
}

