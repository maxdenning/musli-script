#include "Definitions_Arithmetic.h"

static const char* TAG = "Definitions_Arithmetic";

bool exe_ARITHMETIC_ADDITION_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    if( *si_ < 1 ) return printErr( TAG, "exe_ARITHMETIC_ADDITION_", "Only 1 value in stack (si_ < 1)" );

    context_[*ci_].value = stack_[*si_] + stack_[*si_-1];
    spt_ARITHMETIC_CLEANUP_( stack_, si_ );

    (*ci_)++;
    return true;
}

bool exe_ARITHMETIC_SUBTRACT_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    if( *si_ < 1 ) return printErr( TAG, "exe_ARITHMETIC_SUBTRACT_", "Only 1 value in stack (si_ < 1)" );

    #if VALUE_TYPE == float || VALUE_TYPE == double
        if( stack_[*si_] == 0 && stack_[*si_-1] == 0 ){
            context_[*ci_].value = -0.0f;
        }else{
            context_[*ci_].value = stack_[*si_] - stack_[*si_-1];
        }

    #else
        context_[*ci_].value = stack_[*si_] - stack_[*si_-1];

    #endif

    spt_ARITHMETIC_CLEANUP_( stack_, si_ );
    (*ci_)++;
    return true;
}

bool exe_ARITHMETIC_MULTIPLY_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    if( *si_ < 1 ) return printErr( TAG, "exe_ARITHMETIC_MULTIPLY_", "Only 1 value in stack (si_ < 1)" );

    context_[*ci_].value = stack_[*si_] * stack_[*si_-1];
    spt_ARITHMETIC_CLEANUP_( stack_, si_ );

    (*ci_)++;
    return true;
}

bool exe_ARITHMETIC_DIVIDE_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){
    if( *si_ < 1 ) return printErr( TAG, "exe_ARITHMETIC_DIVIDE_", "Only 1 value in stack (si_ < 1)" );
    if( stack_[*si_-1] == 0 ) return printErr(TAG, "exe_ARITHMETIC_DIVIDE_", "Divide by 0 error");

    context_[*ci_].value = stack_[*si_] / stack_[*si_-1];
    spt_ARITHMETIC_CLEANUP_( stack_, si_ );

    (*ci_)++;
    return true;
}

bool exe_ARITHMETIC_MOD_( value_type stack_[], size_t* si_, Word context_[], size_t* ci_ ){ /* !!! */
    if( *si_ < 1 ) return printErr(TAG, "exe_ARITHMETIC_MOD_", "Only 1 value in stack (si_ < 1)");
    if( stack_[*si_-1] == 0 ) return printErr(TAG, "exe_ARITHMETIC_MOD_", "Divide by 0 error");

    context_[*ci_].value = (int)stack_[*si_] % (int)stack_[*si_-1];
    spt_ARITHMETIC_CLEANUP_( stack_, si_ );

    (*ci_)++;
    return true;
}

bool spt_ARITHMETIC_CLEANUP_( value_type stack_[], size_t* si_ ){
    stack_[*si_] = 0;
    stack_[*si_-1] = 0;
    (*si_) -= 2;
    return true;
}

