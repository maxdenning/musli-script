#include "TextHandler.h"

static const char* TAG = "TextHandler";

bool validateArgs(int argc_, char* argv_[] ){
    if(argc_ < 2) return printErr( TAG, "validateArgs", "Invalid number of arguments" );
    return true;
}

bool loadTextFile( const char* file_path, char** out_file_content_, long* out_file_length_ ){
    if( access(file_path, R_OK) == -1 ) return printErr(TAG, "loadTextfile", "Cannot access given file");
    FILE* file = fopen( file_path, "rb" );

    /* get file length */
    fseek(file , 0L , SEEK_END);
    *out_file_length_ = ftell(file);
    rewind(file);

    /* allocate memory for entire content */
    *out_file_content_ = (char*)malloc(*out_file_length_+1);
    if( !*out_file_content_ ){
        fclose(file);
        return printErr(TAG, "loadTextFile", "Memory alloc failed");
    }

    /* copy the file into out_file_content_ */
    if( fread( *out_file_content_, *out_file_length_, 1, file ) != 1 ){
        fclose( file );
        free( *out_file_content_ );
        return printErr( TAG, "loadTextFile", "File read failed" );
    }

    fclose(file);
    return true;
}

void removeWhitespace( char** out_text_, const long* prev_text_length_, long* out_new_length_ ){
    /* remove unwanted white-space characters
    ** remove comments
    */

    long i;
    char clean_text[ *prev_text_length_ ];  /* array containing sanitised string */
    *out_new_length_ = 0;                   /* size of clean_text */


    /* ensure syntax has been initialised */
    if( syntax[ Comment ].symbol    != '%'
     || syntax[ WSDeref ].symbol    != '\\' ) return;


    /* iterate over given text, removing whitespace as appropriate */
    for( i = 0; i < *prev_text_length_; i++ ){
        char* text = &(*out_text_)[i];

        if( *text >= 32 && *text <= 126 ){
            /* only allow character selected range of characters */

            switch( *text ){
            case '%':
                /* COMMENT - move to next NEWLINE character */
                for( i; i < *prev_text_length_; i++ ){
                    if( (*out_text_)[i] == '\n' || (*out_text_)[i] == '\r' ) break;
                }
                break;


            case ' ': break;
            case '\\':
                /* WHITESPACE DEREFEREMCE - handle allowable dereference characters */

                i++;
                text = &(*out_text_)[i];

                switch( *text ){
                case ' ':
                case '\t':
                        clean_text[ *out_new_length_ ] = *text;
                        (*out_new_length_)++;
                    break;
                default: break;
                }

                break;

            default:
                /* not a special character, add to clean text */
                clean_text[ *out_new_length_ ] = *text;
                (*out_new_length_)++;
                break;
            }
        }
    }


    /* exit */
    free( *out_text_ );
    *out_text_ = malloc( (*out_new_length_)+1 );
    memcpy( *out_text_, clean_text, *out_new_length_ ); /* pass back clean_text */
}

